$(document).ready(function() {
    /**
     * IE Polyfill
     */
    
    Number.isInteger = Number.isInteger || function(value) {
    return typeof value === "number" && 
           isFinite(value) && 
           Math.floor(value) === value;
};

    /**
    * Changes value of counter input
    * 
    * @param {string} counter - counter id without '#'
    * @param {integer} direction - increment(+1)/decrement(-1)
    */
   
    function changeCounterValue(counter,direction) {
        var input = $(counter).children('.counter-display').children('.counter-input');
        var value = parseInt(input.val());
        var max = $(input).attr('max');

        if (value == 1 && direction == -1) {
            //not under 1
        } else if (value < 1) {
            input.val(1);
            //not under 1
        } else if (value >= max && direction == 1) {
            input.val(max);
        } else {
            input.val(value+direction); 
        }

        counterCallback(counter);
    }

    /**
     * Set here callback for counter change
     * triggered by user input or changeCounterValue()
     *
     * @param {string} counter - counter id
     */
    
    function counterCallback(counter) {
        var input = $(counter).children('.counter-display').children('.counter-input');
        var value = parseInt(input.val());
        var max = $(input).attr('max');

        if (value == 1) {
            $(counter).children('.counter-button--decrement').attr("disabled", true);
            $(counter).children('.counter-button--increment').removeAttr("disabled");
            $(counter).children('.counter-button--decrement').attr("title", "Dosáhli jste minima");
        } else if (value == max) {
            $(counter).children('.counter-button--increment').attr("disabled", true);
            $(counter).children('.counter-button--decrement').removeAttr("disabled");
            $(counter).children('.counter-button--increment').attr("title", "Dosáhli jste maxima");
        } else {
            $(counter).children('.counter-button').removeAttr("disabled");
            $(counter).children('.counter-button').removeAttr("title");
        }

        //add your callback function here
    }

    /**
     * Actions setup
     *
     * @return {function} counterCallback(#id)
     */

    $('.counter-input').on("change", function(event) {
        var counter = "#"+$(this).parents('.counter').attr('id');
        var input = $(counter).children('.counter-display').children('.counter-input');
        var value = parseInt(input.val());
        var max = $(input).attr('max');

        if (Number.isInteger(value) === false || value < 1) {
            input.val(1);
        } else if (value > max) {
            input.val(max);
        }

        counterCallback(counter);
    });

    $('.counter-button--decrement').click(function(event) {
        changeCounterValue("#"+$(event.target).parents('.counter').attr('id'),-1);
    });

    $('.counter-button--increment').click(function(event) {
        changeCounterValue("#"+$(event.target).parents('.counter').attr('id'),+1);});
});